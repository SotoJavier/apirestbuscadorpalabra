<%-- 
    Document   : listar
    Created on : 08-05-2021, 16:46:32
    Author     : jesoto
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="cl.controller.entity.Consultapalabra"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<Consultapalabra> palabras = (List<Consultapalabra>) request.getAttribute("listarPalabras");
    Iterator<Consultapalabra> itPalabras = palabras.iterator();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Histotico de Busqueda</title>
    </head>
    <bodystyle="background-color:powderblue;">
        <div align ="center">
        <h1>LIstado de Palabras Buscadas</h1>
        <form  name="form" action="ConsultaPalabraController" method="POST">

            <table border="1">
                <thead>
                <th>ID Palabra</th>
                <th>Palabra</th>
                <th>Significado</th>
                <th>Fecha</th>

                </thead>
                <tbody>
                    <%while (itPalabras.hasNext()) {
                                Consultapalabra cm = itPalabras.next();%>
                    <tr>
                        <td id="palabra"><%= cm.getPalabra()%></td>
                        <td id="significado"><%= cm.getSignificado()%></td>
                        <td id="fecha"><%= cm.getFecha()%></td>
                        <td id="id"><%= cm.getId()%></td>

                    </tr>
                    <%}%>                
                </tbody>           
            </table>
            <br><br>

            <button type="submit" name="accion" value="volver" class="btn btn-success">Volver</button>
        </form> 
        </div>     
        <br><br><br><br><br><br><br><br>
        <footer>
            <ul>
                <p><a href="https://developer.oxforddictionaries.com/"><strong>Referencia Oxford Dicctionaries</strong></a></p>
                <p><a href="https://bitbucket.org/SotoJavier/javier.soto.uni01/src/desarrollo/"><strong>Bitbuket Desarrollo</strong></a></p>
                <p><a href="https://bitbucket.org/SotoJavier/javier.soto.uni01/src/master/"><strong>Bitbuket Master</strong></a></p>
                <li><p>Ramo: TALLER DE APLICACIONES EMPRESARIALES</p></li>
                <li><p>Profesor: Cesar Cruces</p></li>
                <li><p>Alumno: Javie Soto Catripan</p></li>
                <li><p>Sección: 50</p></li>
                <li><p>Carrera: Ingenieria en Informatica</p></li>
            </ul>
        </footer>
        &nbsp &nbsp 

    </body>
</html>
