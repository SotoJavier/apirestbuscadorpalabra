<%-- 
    Document   : significado
    Created on : 08-05-2021, 16:32:18
    Author     : jesoto
--%>

<%@page import="cl.controller.entity.Consultapalabra"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    Consultapalabra significado = (Consultapalabra) request.getAttribute("significado");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>SIGNIFICADO</title>
    </head>
    <body style="background-color:powderblue;">
        <div align ="center">
        <h1>SIGNIFICADO DE LA BUSQUEDA</h1>
           <form  name="form" action="ConsultaPalabraController" method="POST">
            <h1><%= significado.getPalabra().toString() %></h1>
            <h2><%= significado.getSignificado().toString() %></h2> 
          <button type="submit" name="accion" value="volver" class="btn btn-success">Volver</button>
         </form>
            </div>     
        <br><br><br><br><br><br><br><br>
        <footer>
            
           <ul>
           <p><a href="https://developer.oxforddictionaries.com/"><strong>Referencia Oxford Dicctionaries</strong></a></p>
           <p><a href="https://bitbucket.org/SotoJavier/javier.soto.uni01/src/desarrollo/"><strong>Bitbuket Desarrollo</strong></a></p>
           <p><a href="https://bitbucket.org/SotoJavier/javier.soto.uni01/src/master/"><strong>Bitbuket Master</strong></a></p>
           <li><p>Ramo: TALLER DE APLICACIONES EMPRESARIALES</p></li>
           <li><p>Profesor: Cesar Cruces</p></li>
           <li><p>Alumno: Javie Soto Catripan</p></li>
           <li><p>Sección: 50</p></li>
           <li><p>Carrera: Ingenieria en Informatica</p></li>
           </ul>
        </footer>
        &nbsp &nbsp 
          
        
    </body>
</html>
