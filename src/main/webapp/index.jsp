<%-- 
    Document   : index
    Created on : 07-05-2021, 22:27:12
    Author     : jesoto
--%>

<%@page import="cl.entity.PalabraEntity"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
         <meta charset="utf-8">
         <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    </head>
    <body style="background-color:powderblue;">
        <div align ="center">
              <h1>Buscador de Palabra!</h1>
              <p><strong>Ingrese palabra a buscar</strong></p>
       <form name="form" action="ConsultaPalabraController" method="POST">
        <p><strong>Palabra</strong><br />
            <span class="wpcf7-form-control-wrap palabra"><input type="text" name="palabra" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" id="palabra" aria-required="true" aria-invalid="false" /></span> </p>
        <p><strong>Fecha</strong><br />
            <span class="wpcf7-form-control-wrap fecha"><input type="date" name="fecha" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" id="fecha" aria-required="true" aria-invalid="false" /></span> </p>
       <!--  <p><strong>Significado</strong><br />
            <span class="wpcf7-form-control-wrap nombre"><input type="text" name="significado" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" id="nombre" aria-required="true" aria-invalid="false" /></span> </p>
           <p><strong>Id</strong><br />
            <span class="wpcf7-form-control-wrap email"><input type="text" name="id" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" id="email" aria-required="true" aria-invalid="false" /></span> </p>
      -->
        <button type="submit" name ="accion" value="ing_palabra" class="btn btn-success">Ingreso Palabra a Buscar</button>
        <button type="submit" name ="accion" value="lis_palabra" class="btn btn-success">Listar Palabras buscadas</button> 
</form> 
        </div>     
        <br><br><br><br><br><br><br><br>
        <footer>
           <ul>
           <p><a href="https://developer.oxforddictionaries.com/"><strong>Referencia Oxford Dicctionaries</strong></a></p>
           <p><a href="https://bitbucket.org/SotoJavier/javier.soto.uni01/src/desarrollo/"><strong>Bitbuket Desarrollo</strong></a></p>
           <p><a href="https://bitbucket.org/SotoJavier/javier.soto.uni01/src/master/"><strong>Bitbuket Master</strong></a></p>
           <li><p>Ramo: TALLER DE APLICACIONES EMPRESARIALES</p></li>
           <li><p>Profesor: Cesar Cruces</p></li>
           <li><p>Alumno: Javie Soto Catripan</p></li>
           <li><p>Sección: 50</p></li>
           <li><p>Carrera: Ingenieria en Informatica</p></li>
           </ul>
        </footer>
        &nbsp &nbsp 
    </body>
</html>
