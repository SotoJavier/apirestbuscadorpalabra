/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.controller;

import cl.controller.dao.ConsultapalabraJpaController;
import cl.controller.entity.Consultapalabra;
import cl.entity.PalabraEntity;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author jesoto
 */
@WebServlet(name = "ConsultaPalabraController", urlPatterns = {"/ConsultaPalabraController"})
public class ConsultaPalabraController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ConsultaPalabraController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ConsultaPalabraController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String accion = request.getParameter("accion");
        String idbuscar = request.getParameter("idbuscar");
        
          if(accion.equals("ing_palabra")) {
            Client client = ClientBuilder.newClient();
              //WebTarget myResource = client.target("https://od-api.oxforddictionaries.com/api/v2/entries/es/" + idbuscar);
              WebTarget myResource = client.target("https://apirestbuscar.herokuapp.com/api/diccionario" + idbuscar);

              PalabraEntity definicion = (PalabraEntity) myResource.request(MediaType.APPLICATION_JSON).get(new GenericType<PalabraEntity>() {
            });
              
                try {
                String palabra = request.getParameter("palabra");
                String significado = request.getParameter("significado");
                String fecha = request.getParameter("fecha");
                String id = request.getParameter("id");
                
                Consultapalabra palabras = new Consultapalabra();
                palabras.setPalabra(palabra);
                palabras.setSignificado(significado);
                palabras.setFecha(fecha);
                palabras.setId(id);
                
                
                
                ConsultapalabraJpaController dao = new ConsultapalabraJpaController();
                
                dao.create(palabras);
                 
               
            } catch (Exception ex) {
                Logger.getLogger(ConsultaPalabraController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
               request.setAttribute("significado", definicion);
               request.getRequestDispatcher("definicion.jsp").forward(request, response);
        }
        if (accion.equals("lis_palabra")){
        Client client = ClientBuilder.newClient();
        //WebTarget myResource = client.target("https://od-api.oxforddictionaries.com/api/v2/entries/es/" + idbuscar);
        WebTarget myResource = client.target("https://apirestbuscar.herokuapp.com/api/diccionario" + idbuscar);
        List<Consultapalabra> lista = (List<Consultapalabra>) myResource.request(MediaType.APPLICATION_JSON).get(new GenericType<List<Consultapalabra>>(){});
        
        request.setAttribute("listarPalabras", lista);
        request.getRequestDispatcher("listar.jsp").forward(request, response);
        }
     
          if (accion.equals("volver")) {
            
            try {
                String palabra = request.getParameter("palabra");
                String definicion = request.getParameter("definicion");
                String fecha = request.getParameter("fecha");
                String id = request.getParameter("id");
                
                Consultapalabra palabras = new Consultapalabra();
                palabras.setPalabra(palabra);
                palabras.setSignificado(definicion);
                palabras.setFecha(fecha);
                palabras.setId(fecha);
                
                
                ConsultapalabraJpaController dao = new ConsultapalabraJpaController();
                
                dao.create(palabras);
                 
               
            } catch (Exception ex) {
                Logger.getLogger(ConsultapalabraJpaController.class.getName()).log(Level.SEVERE, null, ex);
            }
             request.getRequestDispatcher("index.jsp").forward(request, response);
            
        } 
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
