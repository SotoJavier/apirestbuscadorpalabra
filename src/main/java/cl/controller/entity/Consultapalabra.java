/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.controller.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jesoto
 */
@Entity
@Table(name = "consultapalabra")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Consultapalabra.findAll", query = "SELECT c FROM Consultapalabra c"),
    @NamedQuery(name = "Consultapalabra.findByPalabra", query = "SELECT c FROM Consultapalabra c WHERE c.palabra = :palabra"),
    @NamedQuery(name = "Consultapalabra.findBySignificado", query = "SELECT c FROM Consultapalabra c WHERE c.significado = :significado"),
    @NamedQuery(name = "Consultapalabra.findByFecha", query = "SELECT c FROM Consultapalabra c WHERE c.fecha = :fecha"),
    @NamedQuery(name = "Consultapalabra.findById", query = "SELECT c FROM Consultapalabra c WHERE c.id = :id")})
public class Consultapalabra implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "palabra")
    private String palabra;
    @Size(max = 2147483647)
    @Column(name = "significado")
    private String significado;
    @Size(max = 2147483647)
    @Column(name = "fecha")
    private String fecha;
    @Size(max = 2147483647)
    @Column(name = "id")
    private String id;

    public Consultapalabra() {
    }

    public Consultapalabra(String palabra) {
        this.palabra = palabra;
    }

    public String getPalabra() {
        return palabra;
    }

    public void setPalabra(String palabra) {
        this.palabra = palabra;
    }

    public String getSignificado() {
        return significado;
    }

    public void setSignificado(String significado) {
        this.significado = significado;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (palabra != null ? palabra.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Consultapalabra)) {
            return false;
        }
        Consultapalabra other = (Consultapalabra) object;
        if ((this.palabra == null && other.palabra != null) || (this.palabra != null && !this.palabra.equals(other.palabra))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.controller.entity.Consultapalabra[ palabra=" + palabra + " ]";
    }
    
}
