/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.controller.dao;

import cl.controller.dao.exceptions.NonexistentEntityException;
import cl.controller.dao.exceptions.PreexistingEntityException;
import cl.controller.entity.Consultapalabra;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author jesoto
 */
public class ConsultapalabraJpaController implements Serializable {

    public ConsultapalabraJpaController() {
 
    }

    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("contacto_PU");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Consultapalabra consultapalabra) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(consultapalabra);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findConsultapalabra(consultapalabra.getPalabra()) != null) {
                throw new PreexistingEntityException("Consultapalabra " + consultapalabra + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Consultapalabra consultapalabra) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            consultapalabra = em.merge(consultapalabra);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = consultapalabra.getPalabra();
                if (findConsultapalabra(id) == null) {
                    throw new NonexistentEntityException("The consultapalabra with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Consultapalabra consultapalabra;
            try {
                consultapalabra = em.getReference(Consultapalabra.class, id);
                consultapalabra.getPalabra();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The consultapalabra with id " + id + " no longer exists.", enfe);
            }
            em.remove(consultapalabra);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Consultapalabra> findConsultapalabraEntities() {
        return findConsultapalabraEntities(true, -1, -1);
    }

    public List<Consultapalabra> findConsultapalabraEntities(int maxResults, int firstResult) {
        return findConsultapalabraEntities(false, maxResults, firstResult);
    }

    private List<Consultapalabra> findConsultapalabraEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Consultapalabra.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Consultapalabra findConsultapalabra(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Consultapalabra.class, id);
        } finally {
            em.close();
        }
    }

    public int getConsultapalabraCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Consultapalabra> rt = cq.from(Consultapalabra.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
