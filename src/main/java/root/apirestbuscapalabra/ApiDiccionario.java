/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.apirestbuscapalabra;

import cl.controller.dao.ConsultapalabraJpaController;
import cl.controller.entity.Consultapalabra;
import cl.dto.PalabraDTO;
import cl.entity.PalabraEntity;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author jesoto
 */

@Path ("diccionario")
public class ApiDiccionario {
    
    @GET
    @Path("/idbuscar")
    @Produces(MediaType.APPLICATION_JSON)
    public Response significado(@PathParam("idbuscar") String idbuscar) {
        
        try {
            Client client = ClientBuilder.newClient();
            
            WebTarget myResource = client.target("https://od-api.oxforddictionaries.com/api/v2/entries/es/" + idbuscar);
            
            PalabraDTO palabradto = myResource.request(MediaType.APPLICATION_JSON).header("app_key", "31f153c1d2b0bd5376600ff1a35d1e14").header("app_id", "0a054b1c").get(PalabraDTO.class);
            
            Consultapalabra pal = new Consultapalabra();
            
            pal.setPalabra(palabradto.getWord());
            Date fecha = new Date();
            pal.setFecha(fecha.toString());
         //   pal.setSignificado(palabradto.getWord());
            
         String significado = (String) palabradto.getWord(); //.getClass().get(0) getLexicalEntries().get(0).getEntries().get(0).getSenses().get(0).getDefinitions().toString();
       //    String significado = (String) palabradto.getResults().get(0).getLexicalEntries().get(0).getEntries().get(0).getSenses().get(0).getDefinitions().toString();
       
       pal.setSignificado(significado);
            
            ConsultapalabraJpaController dao = new ConsultapalabraJpaController();
            dao.create(pal);  

            return Response.ok(200).entity(pal).build();
        } catch (Exception ex) {
            Logger.getLogger(ApiDiccionario.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }  

    private Object getLexicalEntries() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    }
